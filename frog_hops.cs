using System;
					
public class Program
{
	public static void Main()
	{
		Console.WriteLine("Hello World");
	}
}


// https://community.topcoder.com/stat?c=problem_statement&pm=14912&rd=17164

public class JumpingJackDiv1
{
	///
	///dist is dist per day
	///k is the rep of gap days
	/// n is the number of days to be calculated
	///
	public int getLocationOfJack(int dist, int k, int n)
	{
		int total_dist = 0;
		int cycles = n/k;
		int offset = n%k;
		if ( cycles > 0 )
		{
			total_dist = cycles * dist * (k-1);
		}
		total_dist += (dist * offset);
		
		
		return total_dist;
	}

}

