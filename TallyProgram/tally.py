

# https://www.reddit.com/r/dailyprogrammer/comments/8jcffg/20180514_challenge_361_easy_tally_program/


def get_score(scores_string):
    friends = {'a': 0, 'b': 0, 'c': 0, 'd': 0, 'e': 0}
    for ci in scores_string:
        key = ci.lower()
        if ci.isupper():
            friends[key] -= 1
        elif ci.islower():
            friends[key] += 1
        else:
            print("whoopse, lets fix this later")

    print(friends)


inputz = "abcde", "dbbaCEDbdAacCEAadcB"

if __name__ == '__main__':
    for t in inputz:
        get_score(t)
