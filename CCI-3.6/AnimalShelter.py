from collections import deque

class Animal:

    animal_type = None

    def __init__(self):
        self.name = None
        self.intake_type = None

    def __eq__(self, other):
        return self.animal_type == other.animal_type



class Dog(Animal):

    animal_type = "Dog"

    def __init__(self, dog_name, dog_breed, intake):
        self.name = dog_name
        self.breed = dog_breed
        self.intake_type = intake

class Cat(Animal):

    animal_type = "Cat"

    def __init__(self, cat_name, cat_breed, intake):
        self.name = cat_name
        self.breed = cat_breed
        self.intake_type = intake

class Shelter:

    def open_shelter(self):
        self._animals = deque() #get animals from persistent storage

    def add_animal(self):
        """

        :return:
        """
        #add a cat or dog to the queue
        #intake = #or cat
        #self._animals.append(intake)

    def add_kitty (self):
        cat = Cat("cascat", "siamese", "sadness")
        self._animals.append(cat)

    def add_doge(self):

        dog = Dog("rufus", "lab", "drop-off")
        self._animals.append(dog)

    # needs management of empty set
    def forever_home(self):
        """

        :return:
        """
        #remove from the top of the queue
        ur_new_pet = self._animals.popleft()
        return ur_new_pet


    def get_specific(self, animal_type):
        """

        :param animal_type:
        :return:
        """
        temp_anmimals = deque()
        found_animal = False
        while not found_animal and len(self._animals) > 0:
            temp_ana = self._animals.popleft()
            if temp_ana.animal_type == animal_type:
                found_animal = True
            elif len(self._animals) == 0:
                temp_ana = None
            else:
                temp_anmimals.appendleft(temp_ana)
        self._animals.extendleft(temp_anmimals)
        return temp_ana

def run_shelter():

    johns_shelter = Shelter()
    johns_shelter.open_shelter()
    johns_shelter.add_doge()
    johns_shelter.add_doge()
    johns_shelter.add_doge()
    johns_shelter.add_kitty()
    print(johns_shelter.get_specific("Cat"))
    print(johns_shelter.forever_home())
    print(johns_shelter._animals)



if __name__ == '__main__':
    run_shelter()